
---------------- Information for assembly 'WA_38-hap2_scaffolds_final.fa' ----------------


                                         Number of scaffolds        229
                                     Total size of scaffolds  667178241
                                            Longest scaffold   56305675
                                           Shortest scaffold       1000
                                 Number of scaffolds > 1K nt        227  99.1%
                                Number of scaffolds > 10K nt        226  98.7%
                               Number of scaffolds > 100K nt         47  20.5%
                                 Number of scaffolds > 1M nt         18   7.9%
                                Number of scaffolds > 10M nt         17   7.4%
                                          Mean scaffold size    2913442
                                        Median scaffold size      45937
                                         N50 scaffold length   37154807
                                          L50 scaffold count          8
                                                 scaffold %A      30.85
                                                 scaffold %C      19.14
                                                 scaffold %G      19.15
                                                 scaffold %T      30.86
                                                 scaffold %N       0.00
                                         scaffold %non-ACGTN       0.00
                             Number of scaffold non-ACGTN nt          0

                Percentage of assembly in scaffolded contigs      16.3%
              Percentage of assembly in unscaffolded contigs      83.7%
                      Average number of contigs per scaffold        1.0
Average length of break (>25 Ns) between contigs in scaffold        200

                                           Number of contigs        233
                              Number of contigs in scaffolds          7
                          Number of contigs not in scaffolds        226
                                       Total size of contigs  667177441
                                              Longest contig   56305675
                                             Shortest contig       1000
                                   Number of contigs > 1K nt        231  99.1%
                                  Number of contigs > 10K nt        230  98.7%
                                 Number of contigs > 100K nt         51  21.9%
                                   Number of contigs > 1M nt         21   9.0%
                                  Number of contigs > 10M nt         20   8.6%
                                            Mean contig size    2863422
                                          Median contig size      46645
                                           N50 contig length   35835625
                                            L50 contig count          8
                                                   contig %A      30.85
                                                   contig %C      19.14
                                                   contig %G      19.15
                                                   contig %T      30.86
                                                   contig %N       0.00
                                           contig %non-ACGTN       0.00
                               Number of contig non-ACGTN nt          0
