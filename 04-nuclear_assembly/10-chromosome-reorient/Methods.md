Although MUMmer helped us determined which scaffold from our WA 38 assembly matched which chromosome in the Gala assembly, dotplots from Assemblitics did not tell us the direction of chromosomes. 

To determine the whether the WA 38 chromosomes are oriented the same as all the other apples, we ran LASTZ on Geneious 9. Same chromosomes from haplomes of 'WA 38' were aligned to 'Gala' HapA with the following parameters: 
	Step length: 10,000
	Seed Pattern: 12 of 19
	Perform chaining: check
	Search strand: both
	HSP Threshold Score (upper limit): 3000

According to the results from alignment, the following chromosomes were inverted using the Reverse Complimemnt option in Geneious 9.
chr1A
chr1B
chr2B
chr6A
chr6B
chr8B
chr9A
chr9B
chr10A
chr10B
chr11A
chr11B
chr14A
chr14B
chr15A
chr15B
chr17A

The re-oritented sequenced were aligned to the 'Gala' hapA chromosomes again for a last sanity check. This time, same parameters as list above were used, except Search strand was limited to Forward only.

The final sequences were exported from Geneious and named as Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa and Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa and were uploaded to `/scidas/instruction/HORT503-Fall2022/wa-38-wgaa-official_files/04-nuclear_assembly/10-reorient/`
