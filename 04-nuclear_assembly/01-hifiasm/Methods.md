# hifisam assembly

To assemble each haplome of the WA-38 using Hifi and Hi-C reads. 

---

Create symbolic links to the PacBio long reads. 

```bash
ln -s ../../01-input_data/PacBio/m64233e_211117_203327.fastq.gz
ln -s ../../01-input_data/PacBio/m64233e_211123_195103.fastq.gz
```

Create symbolic linkes to the Hi-C data. 


```bash
ln -s ../../01-input_data/Illumina/Hi-C/JNQM_OmniC_NA_NA_CTTGTCGA_Apple_Cosmic_Crisp-Apple_WA38_Cosmic_OmniC_I1161L1_L1_R2.fastq
ln -s ../../01-input_data/Illumina/Hi-C/JNQM_OmniC_NA_NA_CTTGTCGA_Apple_Cosmic_Crisp-Apple_WA38_Cosmic_OmniC_I1161L1_L1_R1.fastq
```

Run the hifasm program
```bash
sbatch 01-hifiasm.srun
```

# File generation

Hifiasm does not create FASTA files of our assemblies. Instead it genrates GFA files. We
can use `awk` to do this. Awk is an essential tool for data wrangling on the command-line.
You can often do the same with awk on a single command-line that would take multiple  lines
of a Python code. We do not have time to learn about Awk in class. If you want that, take 
The 3rd unit of the Spring AFS 505 course. We will use awk to parse the GFA files output
by hifiasm to extract the contigs and generate a FASTA file. We will do this for the primary
assembly and the the two haplotype assemblies. The instructions to do this were found here: 
https://hifiasm.readthedocs.io/en/latest/faq.html#how-do-i-get-contigs-in-fasta

```bash
awk '/^S/{print ">"$2;print $3}' WA_38.asm.hic.p_ctg.gfa > WA_38.asm.hic.p_ctg.fa
awk '/^S/{print ">"$2;print $3}' WA_38.asm.hic.hap1.p_ctg.gfa > WA_38.asm.hic.hap1.p_ctg.fa
awk '/^S/{print ">"$2;print $3}' WA_38.asm.hic.hap2.p_ctg.gfa > WA_38.asm.hic.hap2.p_ctg.fa
```

# Assembly statistics
Next we want to find some statistics about our assembly.  Years ago, a group of folks
got together in an event call Assemblathon. At this event some code was written, including
a Perl script that reads in FASTA files from an assembly and calculates those important
stats. But first, we need to download the script and make it executable.

```bash
wget https://raw.githubusercontent.com/KorfLab/Assemblathon/master/assemblathon_stats.pl
wget http://korflab.ucdavis.edu/Unix_and_Perl/FAlite.pm
chmod 755 assemblathon_stats.pl
```

Now run the assemblathon stats script.
```bash
./assemblathon_stats.pl WA_38.asm.hic.p_ctg.fa > WA_38.asm.hic.p_ctg.stats.txt
./assemblathon_stats.pl WA_38.asm.hic.hap1.p_ctg.fa > WA_38.asm.hic.hap1.p_ctg.stats.txt
./assemblathon_stats.pl WA_38.asm.hic.hap2.p_ctg.fa > WA_38.asm.hic.hap2.p_ctg.stats.txt
```

# BUSCO

Last, run BUSCO to estimate assembly completeness
```bash
sbatch 02-busco-hapA.srun
sbatch 03-busco-hapB.srun
```
