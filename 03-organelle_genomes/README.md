Directory Overview
==================
This directory includes steps to assemble and annotation chloroplast and mitochondrion.  

Authors: Huiting Zhang  
Last updated: Dec 20th 2023  

Directory Structure
===================
The following describes the purpose for each directory:
* 01-chloroplast  
    - 01-NOVOPlasty  
    This directiry contains methods used to assemble the chloroplast.
    -  02-annotation  
    This directory contains steps to annotate and visualize the chloroplast.  

* 02-mitochondrion
    - 01-assembly_mitoHiFi  
    This directory contains methods used to assemble the mitochondrion genome using HiFi reads.  
    - 02-annotation  
    This directory contains description on annotation and visualization of the mitochondrion genome.
