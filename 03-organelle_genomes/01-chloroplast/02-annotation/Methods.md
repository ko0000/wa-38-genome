### Annotation with GeSeq

To annotate the Choloroplast we will use [GeSEq](https://chlorobox.mpimp-golm.mpg.de/geseq.html)

Once finished, download all results as a zip file and place here.

Settings:
- FASTA File(s) to annotate:
  - Upload the NOVOPlasty circular chloroplast genome. Select "circular"
  - Sequence source: Plastid (land plants)
  - Annotation options (select):
    - Annotate plastid inverted Repeat (IR)
    - Annotate plasted trans-speliced rps12
  - Annotation Support (select):
    - Support annotation by Chloe
  - Annotation revision (select):
    - Keep best annotation only

Annotation:
- Blat search
  - Protein search identity: 25
  - rRNA, tRNA, DNA search identity: 85
  - Annotate (select): CDS, tRNA, rRNA
  - Options (select):
    - Ignore genes annotated as locus tag
-3rd Party tRNA annotations (select)
  - Aragorn v1.2.38
    - Genetic Code: Bacterial/Plant Choloroplast
    - Max intron length: 3000

Blat Reference Sequence:
- MPI-MP Reference Set (Select):
  - Chloroplast land plants (CDS+rRNA)

3rd Party Stand Alone Annotators
- Chloe v0.1.0
  - Annotate (select): CDS, rRNA, rRNA

Output Options
- Generate multi-GenBank
- Generate multi-GFF3

### Annotation curation and visualization
Examination of the annotation plot generated from last step showed repeated annotations of the same gene at the same location(rrn23, trnR-ACG, trnP-UGG, trnF-GAA, trnM-CAU, trnG-GCC, trnD-GUC, trnH-GUG). Close examination of the GenBank annotation file showed that those tRNA annotation come from Chloe and ARAGORN and the gene coordinates from those 2 methods differed by one or two bases. To streamline the final figure, ARAGORN annotation of those repetative genes were removed from the GenBank file. The rrn23 were annotated by both blatN and Chloe and the gene cooridinates differed by one base. The blatN lines were removed.  
The edited GenBank file was uploaded to [OGDRAW version 1.3.1](https://chlorobox.mpimp-golm.mpg.de/OGDraw.html) to generate the chloroplast annotation figure.   