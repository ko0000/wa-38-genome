### Annotation with GeSeq

To annotate the mitochondria we will use [GeSEq](https://chlorobox.mpimp-golm.mpg.de/geseq.html)  

Once finished, download all results as a zip file and place here.  

Settings (default):  
- FASTA File(s) to annotate:  
  - Upload the mitoHiFi circular mitochondria genome. Select "circular"  
  - Sequence source: Mitochondrial  
  - Annotation revision (select):  
    - Keep best annotation only  

Annotation:  
- Blat search  
  - Protein search identity: 25  
  - rRNA, tRNA, DNA search identity: 85  
  - Annotate (select): CDS, tRNA, rRNA  
  - Options (select):  
    - Ignore genes annotated as locus tag  

Blat Reference Sequence:  
- 3rd Party References:  
    - click Add NCBI RefSeq(s). In the new window, select Mitochondrion and search for NC_018554.1. Click *Add* next to *Malus domestica*.  

Output Options
- Generate multi-GenBank
- Generate multi-GFF3

### Annotation curation and visualization
Several genes are fragmented and are manually removed from the annotation. 
The edited GenBank file was uploaded to [OGDRAW version 1.3.1](https://chlorobox.mpimp-golm.mpg.de/OGDraw.html) to generate the mitochondrion annotation figure. 