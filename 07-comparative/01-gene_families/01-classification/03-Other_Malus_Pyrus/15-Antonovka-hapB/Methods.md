Protein sequences were downloaded from GDR
```bash
wget https://www.rosaceae.org/rosaceae_downloads/Malus_x_domestica/Antonovka_172670-B_v1.0/genes/Antonovka_hapolomeB_pep.fa.gz
gunzip Antonovka_hapolomeB_pep.fa.gz
```

Since we are not doing any phylogenetic analysis, we will only run the classifier on the protein sequences.

```bash
sbatch 01-PlantTribes_orthogroup_classifier.srun
```

Run annotation BUSCO
```bash
sbatch 02-annotation_BUSCO.srun
```
