Protein sequences were downloaded from GDR
```bash
wget https://www.rosaceae.org/rosaceae_downloads/Pyrus_communis/pcommunis_DAnjou_v2.3/genes/Pyrus_hap2_v2.3.a1.all.maker.proteins.fasta.gz
gunzip Pyrus_hap2_v2.3.a1.all.maker.proteins.fasta.gz
```

Since this annotation only has matching transcripts and protein sequences, rather than mathching coding and protein sequences, we will only run the classifier on the protein sequences.

```bash
sbatch 01-PlantTribes_orthogroup_classifier.srun
```

