Download the protein sequences from GDR and unzip the .gz file to a fasta file.
```bash
wget https://www.rosaceae.org/rosaceae_downloads/Malus_baccata/mbaccata_v1.0/genes/Malus_baccata_v1.0_pep.fasta.gz
wget https://www.rosaceae.org/rosaceae_downloads/Malus_baccata/mbaccata_v1.0/genes/Malus_baccata_v1.0_cds.fasta.gz
gunzip Malus_baccata_v1.0_pep.fasta.gz
gunzip Malus_baccata_v1.0_cds.fasta.gz
```

Run PlantTribes2 geneFamilyClassifier with the 26Gv2.0 database
```bash
sbatch 01-PlantTribes_orthogroup_classifier.srun
```
