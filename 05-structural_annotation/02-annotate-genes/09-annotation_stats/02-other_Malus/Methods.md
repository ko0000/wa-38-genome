### Data acquisition

Download protein and GFF files for all published Malus domestica genomes
```bash
#Antonovka
wget https://www.rosaceae.org/rosaceae_downloads/Malus_x_domestica/Antonovka_172670-B_v1.0/genes/Antonovka_hapolomeA_pep.fa.gz
gunzip Antonovka_hapolomeA_pep.fa.gz
wget https://www.rosaceae.org/rosaceae_downloads/Malus_x_domestica/Antonovka_172670-B_v1.0/genes/Antonovka_hapolomeB_pep.fa.gz
gunzip Antonovka_hapolomeB_pep.fa.gz
wget https://www.rosaceae.org/rosaceae_downloads/Malus_x_domestica/Antonovka_172670-B_v1.0/genes/Antonovka_hapolomeA.gff3.gz
gunzip Antonovka_hapolomeA.gff3.gz
wget https://www.rosaceae.org/rosaceae_downloads/Malus_x_domestica/Antonovka_172670-B_v1.0/genes/Antonovka_hapolomeB.gff3.gz
gunzip Antonovka_hapolomeB.gff3.gz
#Gala diploid
wget https://www.rosaceae.org/rosaceae_downloads/Malus_x_domestica/Gala_diploid_v1/genes/Gala_diploid_v2.pep.fa.gz
gunzip Gala_diploid_v2.pep.fa.gz
wget https://www.rosaceae.org/rosaceae_downloads/Malus_x_domestica/Gala_diploid_v1/genes/Gala_diploid_v2.gff.gz
gunzip Gala_diploid_v2.gff.gz
#HFTH (the TH version is used instead of the most recent version because the TH version has better annotation BUSCO)
wget https://www.rosaceae.org/rosaceae_downloads/Malus_x_domestica/Malus_x_domestica_HFTH1_v1.0.a1/genes/HFTH1.gene.pep.fa.gz
gunzip HFTH1.gene.pep.fa.gz
wget https://www.rosaceae.org/rosaceae_downloads/Malus_x_domestica/Malus_x_domestica_HFTH1_v1.0.a1/genes/HFTH1.gene.gff3.gz
gunzip HFTH1.gene.gff3.gz
#Honeycrisp
wget https://www.rosaceae.org/rosaceae_downloads/Malus_x_domestica/md_Honeycrisp_v1.0/genes/g.Honeycrisp_HAP1_braker1+2_combined_fullSupport_longname_filtered.pep.fa.gz
gunzip g.Honeycrisp_HAP1_braker1+2_combined_fullSupport_longname_filtered.pep.fa.gz
wget https://www.rosaceae.org/rosaceae_downloads/Malus_x_domestica/md_Honeycrisp_v1.0/genes/g.Honeycrisp_HAP2_braker1+2_combined_fullSupport_longname_filtered.pep.fa.gz
gunzip g.Honeycrisp_HAP2_braker1+2_combined_fullSupport_longname_filtered.pep.fa.gz
wget https://www.rosaceae.org/rosaceae_downloads/Malus_x_domestica/md_Honeycrisp_v1.0/genes/Honeycrisp_HAP1_braker1+2_combined_fullSupport_renamed_filtered_testNoStartNoStop_added_long_short_names.gff3.txt.gz
gunzip Honeycrisp_HAP1_braker1+2_combined_fullSupport_renamed_filtered_testNoStartNoStop_added_long_short_names.gff3.txt.gz
wget https://www.rosaceae.org/rosaceae_downloads/Malus_x_domestica/md_Honeycrisp_v1.0/genes/Honeycrisp_HAP2_braker1+2_combined_fullSupport_renamed_filtered_testNoStartNoStop_added_long_short_names.gff3.txt.gz
gunzip Honeycrisp_HAP2_braker1+2_combined_fullSupport_renamed_filtered_testNoStartNoStop_added_long_short_names.gff3.txt.gz
#GDDH13
wget https://www.rosaceae.org/rosaceae_downloads/Malus_x_domestica/Malus_x_domestica-genome_GDDH13_v1.1/genes/GDDH13_1-1_prot.fasta.gz
gunzip GDDH13_1-1_prot.fasta.gz
wget https://www.rosaceae.org/rosaceae_downloads/Malus_x_domestica/Malus_x_domestica-genome_GDDH13_v1.1/genes/gene_models_20170612.gff3.gz
gunzip gene_models_20170612.gff3.gz
```
A few of the files are renamed so that files of the same type has a similar naming structure
```bash
mv GDDH13_1-1_prot.fasta GDDH13_1-1_prot.fa
mv Gala_diploid_v2.gff Gala_diploid_v2.gff3
mv Honeycrisp_HAP1_braker1+2_combined_fullSupport_renamed_filtered_testNoStartNoStop_added_long_short_names.gff3.txt Honeycrisp_hap1.gff3
mv Honeycrisp_HAP2_braker1+2_combined_fullSupport_renamed_filtered_testNoStartNoStop_added_long_short_names.gff3.txt Honeycrisp_hap2.gff3
mv gene_models_20170612.gff3 GDDH13.gff3
mv g.Honeycrisp_HAP1_braker1+2_combined_fullSupport_longname_filtered.pep.fa Honeycrisp.hapA.pep.fa
mv g.Honeycrisp_HAP2_braker1+2_combined_fullSupport_longname_filtered.pep.fa Honeycrisp.hapB.pep.fa
```

### Protein length calculation

```bash
for i in *.fa; do echo "$i,""$(../../../../01-input_data/bioawk/bioawk -c fastx '{ print $name, length($seq)}' $i | awk '{x+=$2}END{print x/NR}')"; done
# Antonovka_hapolomeA_pep.fa,398.718
# Antonovka_hapolomeB_pep.fa,396.279
# Gala_diploid_v2.pep.fa,408.932
# GDDH13_1-1_prot.fa,374.721
# HFTH1.gene.pep.fa,392.641
# Honeycrisp.hapA.pep.fa,376.903
# Honeycrisp.hapB.pep.fa,360.382
```

### splice variants calculation

Appearantly GDDH13, HFTH, and Gala annotation only contains one transcript per gene. So splice variants count is only performed in Antonovka and Honeycrisp.
```bash
#Antonovka
grep ">" Antonovka_hapolomeA_pep.fa | sed 's/>//' | sed 's/ .*//' | sed 's/0\./\t/' | cut -f2 | sort | uniq -c
#  45200 1 -- this means all the numbers after . is 1, there are no t2, t3. etc
grep ">" Antonovka_hapolomeB_pep.fa | sed 's/>//' | sed 's/ .*//' | sed 's/0\./\t/' | cut -f2 | sort | uniq -c
#  44969 1
# no splice variants annotation in Antonovka

# Honeycrisp
grep ">" Honeycrisp.hapB.pep.fa | sed 's/>//' | sed 's/\.t/\tt/' | cut -f1 | sort | uniq -c | sed 's/Maldo/\tMaldo/' | awk '{print $2"\t"$1}' | awk '{total += $2; count++} END {print total/count}'
#1.06865
grep ">" Honeycrisp.hapA.pep.fa | sed 's/>//' | sed 's/\.t/\tt/' | cut -f1 | sort | uniq -c | sed 's/Maldo/\tMaldo/' | awk '{print $2"\t"$1}' | awk '{total += $2; count++} END {print total/count}'
#1.05344
```


### UTR calculation

Antonovka, Honeycrisp have no UTR annotation

```bash
#Gala
grep "UTR" Gala_diploid_v2.gff3 | cut -f9 | sed 's/ID=//' | sed 's/;.*//' | sed 's/:.*//' | sort | uniq | wc -l
# 17679
cut -f3 Gala_diploid_v2.gff3 | grep -c "gene" 
#90507

#GDDH13
grep "UTR" GDDH13.gff3 | cut -f9 | sed 's/;/\t/g' | cut -f2 | uniq | wc -l
#32760
grep -c ">" GDDH13_1-1_prot.fa 
#45116

#HFTH
grep "UTR" HFTH1.gene.gff3 | cut -f9 | sort | uniq | wc -l
#42
cut -f3 HFTH1.gene.gff3 | grep -c "gene"
#44677
```
