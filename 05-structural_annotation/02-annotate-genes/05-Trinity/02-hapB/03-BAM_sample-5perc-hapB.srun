#!/bin/bash
#SBATCH --partition=ficklin_class
#SBATCH --account=ficklin_class
#SBATCH --job-name=samtools_sample_5perc
#SBATCH --output=%x_%j.out
#SBATCH --error=%x_%j.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user=stephen.ficklin@wsu.edu
#SBATCH --time=7-00:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=20

module add singularity

# Limit the reads to only 5% as the large combined file is too large.
singularity exec -B ${PWD} docker://systemsgenetics/actg-wgaa-samtools:1.15.1 \
  samtools view --threads 20 -s 0.05 -b RNAseq_all-vs-WA_38-hapB.bam > RNAseq_all-vs-WA_38-hapB.5perc.bam

# Sort the BAM file
singularity exec -B ${PWD} docker://systemsgenetics/actg-wgaa-samtools:1.15.1 \
  samtools sort RNAseq_all-vs-WA_38-hapB.5perc.bam > RNAseq_all-vs-WA_38-hapB.5perc.sorted.bam

# Index the BAM file
singularity exec -B ${PWD} docker://systemsgenetics/actg-wgaa-samtools:1.15.1 \
  samtools index  RNAseq_all-vs-WA_38-hapB.5perc.sorted.bam
