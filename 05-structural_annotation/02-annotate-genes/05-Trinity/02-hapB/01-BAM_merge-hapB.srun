#!/bin/bash
#SBATCH --partition=ficklin_class
#SBATCH --account=ficklin_class
#SBATCH --job-name=samtools_merge
#SBATCH --output=%x_%j.out
#SBATCH --error=%x_%j.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user=stephen.ficklin@wsu.edu
#SBATCH --time=7-00:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=20

module add singularity

# Create one large BAM file using all of the aligned RNA-seq reads
singularity exec -B ${PWD} docker://systemsgenetics/actg-wgaa-samtools:1.15.1 \
  samtools merge -f --threads 20 -o RNAseq_all-vs-WA_38-hapB.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/JLHJ_RNAseq_002B_Fruitlet_Stage_1_ACTAAGAT_Apple_WA38_I1126_L1_R/JLHJ_RNAseq_002B_Fruitlet_Stage_1_ACTAAGAT_Apple_WA38_I1126_L1_R.sorted.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/JLHK_RNAseq_008C_Fruitlet_Stage_2_GTCGGAGC_Apple_WA38_I1126_L1_R/JLHK_RNAseq_008C_Fruitlet_Stage_2_GTCGGAGC_Apple_WA38_I1126_L1_R.sorted.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/JLHL_RNAseq_0013_Budding_Leaves_CTTGGTAT_Apple_WA38_I1126_L1_R/JLHL_RNAseq_0013_Budding_Leaves_CTTGGTAT_Apple_WA38_I1126_L1_R.sorted.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/JLHM_RNAseq_0018_Expanding_Leaves_TCCAACGC_Apple_WA38_I1126_L1_R/JLHM_RNAseq_0018_Expanding_Leaves_TCCAACGC_Apple_WA38_I1126_L1_R.sorted.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/JLHN_RNAseq_0023C_Roots_from_tissue_culture_CCGTGAAG_Apple_WA38_I1126_L1_R/JLHN_RNAseq_0023C_Roots_from_tissue_culture_CCGTGAAG_Apple_WA38_I1126_L1_R.sorted.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/JLHP_RNAseq_0026_Quarter_inch_Buds_TTACAGGA_Apple_WA38_I1126_L1_R/JLHP_RNAseq_0026_Quarter_inch_Buds_TTACAGGA_Apple_WA38_I1126_L1_R.sorted.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/JLHQ_RNAseq_0031_Flower_Buds_GGCATTCT_Apple_WA38_I1126_L1_R/JLHQ_RNAseq_0031_Flower_Buds_GGCATTCT_Apple_WA38_I1126_L1_R.sorted.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/JLHR_RNAseq_0036B_Open_Buds_AATGCCTC_Apple_WA38_I1126_L1_R/JLHR_RNAseq_0036B_Open_Buds_AATGCCTC_Apple_WA38_I1126_L1_R.sorted.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/SRR20171490.sra/SRR20171490.sra.sorted.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/SRR20171491.sra/SRR20171491.sra.sorted.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/SRR20171492.sra/SRR20171492.sra.sorted.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/SRR20171493.sra/SRR20171493.sra.sorted.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/SRR20171494.sra/SRR20171494.sra.sorted.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/SRR20171495.sra/SRR20171495.sra.sorted.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/SRR20171496.sra/SRR20171496.sra.sorted.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/SRR20171497.sra/SRR20171497.sra.sorted.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/SRR20171498.sra/SRR20171498.sra.sorted.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/SRR20171499.sra/SRR20171499.sra.sorted.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/SRR20171500.sra/SRR20171500.sra.sorted.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/SRR20171501.sra/SRR20171501.sra.sorted.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/SRR20171502.sra/SRR20171502.sra.sorted.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/SRR20171506.sra/SRR20171506.sra.sorted.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/SRR20171512.sra/SRR20171512.sra.sorted.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/SRR20171513.sra/SRR20171513.sra.sorted.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/SRR20171514.sra/SRR20171514.sra.sorted.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/SRR20171515.sra/SRR20171515.sra.sorted.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/SRR20171516.sra/SRR20171516.sra.sorted.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/SRR20171517.sra/SRR20171517.sra.sorted.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/SRR20171518.sra/SRR20171518.sra.sorted.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/SRR20171519.sra/SRR20171519.sra.sorted.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/SRR20171520.sra/SRR20171520.sra.sorted.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/SRR20171521.sra/SRR20171521.sra.sorted.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/SRR20171522.sra/SRR20171522.sra.sorted.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/SRR20171523.sra/SRR20171523.sra.sorted.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/SRR20171524.sra/SRR20171524.sra.sorted.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/SRR20171525.sra/SRR20171525.sra.sorted.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/SRR20171526.sra/SRR20171526.sra.sorted.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/SRR20171527.sra/SRR20171527.sra.sorted.bam \
    ../../01-GEMmaker/02-hapB/results/Samples/SRR20171528.sra/SRR20171528.sra.sorted.bam

