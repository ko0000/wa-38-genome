#!/usr/bin/env python3

"""
A script written for the WSU Hort 403/503 course for assembly of WA 38

This script takes in a GFF file created during the TSEBRA step and the
GFF file of repeats from RepeatMasker. It removes genes that were
predicted in repeat regions.

Items top fix:

1.  Overlap of repeat regions in CDS looks incorrect.
    Maldo.cc.v1a1.chr1A.g000100
    Maldo.cc.v1a1.chr1A.g005060

2.  Redo the bad transcripts filter. Change it so that it fixes the bad model
    by breaking the gene model into two and putting the transcript that overlaps with
    no other transcripts into it's own model. Name if fixBadModels.  Perform this first
    before any other filtering.
    Maldo.cc.v1a1.chr1A.g000760 (case where splice variants of single exons which is okay)
    Maldo.cc.v1a1.chr1A.g018660 (should be split into two gene models)

3.  Gene overlap: if the genes are on different directions should we remove one?
    Maldo.cc.v1a1.chr1A.g004430 & Maldo.cc.v1a1.chr1A.g004440

"""

import re
import pandas as pd
import argparse
import copy
import os.path
import pickle
from os.path import exists

# Requires package installation: biopython, bcbio-gff
from Bio.Seq import Seq
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from BCBio import GFF
from Bio.SeqFeature import SeqFeature, FeatureLocation
from Bio.SeqIO.FastaIO import FastaWriter
from Bio import BiopythonWarning
from operator import itemgetter, attrgetter


import pysam

SeqRecords = {}

def parseGenome(genome):
    """
    Indexes the sequences in the assembly for quick lookup.

    Parameters
    ----------
    genome : array
        An array of SeqRecords.

    Returns
    -------
    None.

    """

    for rec in genome:
        SeqRecords[rec.id] = rec


def getSeqRecord(seqid):
    """
    Returns the seqRecord for a sequence in the FASTA file

    Parameters
    ----------
    seqid : string
        The name of the landmark chromosome or scaffold..

    Returns
    -------
    SeqRecord
        A SeqRecord object for the given chromsome/scaffold sequence..

    """

    if seqid in SeqRecords:
        return SeqRecords[seqid]
    print("Could not find sequence, '{}', in the FASTA file".format(seqid))
    exit(1)


def parseGFF(gff_file):
    """    
    Parses a GFF file and returns an array of SeqRecord objects.

    Parameters
    ----------
    genes_in : file object
        File object for the GFF file.

    Returns
    -------
    seq_recs : array
        An array of SeqRecord objects.

    """
    genes = GFF.parse(gff_file)
    
    seq_recs = []
    for rec in genes:
        seq_rec = SeqRecord(seq=Seq(""), id=rec.id)
        for feature in rec.features:
            seq_rec.features.append(feature)            
        seq_recs.append(seq_rec)
        
    return seq_recs



def getGeneLocs(seq_recs, bam_in):
    """
    Generates a dictionary of start/stop coordinates for genes.

    Parameters
    ----------
    seq_recs : array
        An array of SeqRecords containing gene features.

    Returns
    -------
    gene_locs : dict
        A dictionary of gene positions.

    """        
    gene_locs = {}

    for rec in seq_recs:
        seqid = rec.id
        
        # Iterate through the features (i.e. genes).
        for feature in rec.features:
            if feature.type == 'gene':
                gene = feature
                
                # The UTRs may not be correct, so we'll limit the gene 
                # to the CDS size. Initialize with the reverse.
                start = int(gene.location.end)
                end = int(gene.location.start)
                max_cds_len = 0
                
                # Whats the CDS length of this gene?
                for subfeature in gene.sub_features:
                    if subfeature.type == 'mRNA':
                        mRNA = subfeature
                        cds_len = 0
                        for subsubfeature in mRNA.sub_features:
                            if subsubfeature.type == 'CDS':
                                cds = subsubfeature
                                
                                # Calculate the length of the CDS
                                cds_start = int(cds.location.start)
                                cds_end = int(cds.location.end)
                                cds_len = cds_len + (cds_end - cds_start) 
                                if cds_len > max_cds_len:
                                    max_cds_len = cds_len
                                
                                # Keep the leftmost and rightmost CDS start/end
                                if cds_start < start:
                                    start = cds_start
                                if cds_end > end:
                                    end = cds_end
                
                # Store the coordinates of every gene
                if seqid not in gene_locs.keys():
                    gene_locs[seqid] = {}
                if start not in gene_locs[seqid].keys():
                    gene_locs[seqid][start] = {}
                if end not in gene_locs[seqid][start].keys():
                    gene_locs[seqid][start][end] = []                           
                gene_locs[seqid][start][end].append(gene.id)  
                
                # How many reads support this gene?
                num_reads = bam_in.count(seqid, start, end)                                                                 

                print([seqid, start, end, num_reads, cds_len])
                
                # Store details about each gene.
                gene_locs[gene.id] = [seqid, start, end, num_reads, cds_len]
                
                
    return gene_locs


def filterDuplicateTranscripts(args, seq_recs, log):
    """
    Some of the gene models have splice variants that are identical.
    In cases where all exons are the same, we we remove the duplicate.

    Parameters
    ----------
    args : args object
        Command-line arguments for this program.
    seq_recs : array
        An array of SeqRecords containing gene features.
    gene_locs : dict
        A dictionary of gene positions.
    log : file object
        A file handle for the output log.

    Returns
    -------
    fseq_recs : array
        An array of SeqRecords containing gene features.
    """

    fseq_recs = []
    kept_genes = set()
    for rec in seq_recs:
        # Get the landmark sequence record.
        seqid = rec.id
        print("Checking genes for duplicate transcripts in '{}'...".format(seqid))
        seq_rec = copy.deepcopy(getSeqRecord(seqid))

        # Iterate through the features (i.e. genes).
        for feature in rec.features:
            if feature.type == 'gene':                
                gene = feature # type: Bio.SeqFeature.SeqFeature
                transcript_strs = pd.Series(dtype=str)               

                # Iterate through the subfeatures of the gene.
                for subfeature in gene.sub_features:
                    if subfeature.type == 'mRNA':
                        mRNA = subfeature
                        exons_str = "{}-".format(gene.id)

                        # Iterate through the subfeatures of the mRNA                        
                        for subsubfeature in mRNA.sub_features:
                            if subsubfeature.type == 'exon':  
                                exon = subsubfeature  
                                exons_str = "{}{}".format(exons_str, exon.location.start)
                        
                        # Save the string of exons start/stops, any duplicates
                        # will be identical.
                        transcript_strs = pd.concat([transcript_strs, pd.Series(exons_str)])
                
                transcript_strs = transcript_strs.reset_index(drop=True)

                # Get the indexes of the duplicated transcripts
                duplicates = transcript_strs.duplicated();
                duplicates = duplicates.index[duplicates == True]                

                # Create a new gene record without the duplicated transcripts.
                new_gene = SeqFeature(location=gene.location, type=gene.type, 
                                      strand=gene.strand, id=gene.id,
                                      ref=gene.ref, qualifiers=gene.qualifiers) 
                new_gene.sub_features = []

                # Iterate through the subfeatures (transcripts) and 
                # keep those that are not duplicated.
                mRNA_index = 0                
                for subfeature in gene.sub_features:
                    if subfeature.type == 'mRNA':
                        if mRNA_index not in duplicates:
                            new_gene.sub_features.append(subfeature)
                        else:
                            log.write("Removing duplicate mRNA {}\n".format(subfeature.id))
                        mRNA_index = mRNA_index + 1     
        
            seq_rec.features.append(new_gene)     

        # Add this landmark sequence to the full list.
        fseq_recs.append(seq_rec)

    # Return our new list of landmark sequences with features.
    return fseq_recs

def fixGenes(seq_recs, ignore, log):
    """
    Fixes gene models after filtering. For example, if transcripts were
    removed from a gene then the size may be different. Also, we want
    transcripts to be numered numerically. If some were removed then 
    we'll have missing numbers.

    Parameters
    ----------
    seq_recs : array
        An array of SeqRecords containing gene features.
    ignore: array
        An array containing the names of scaffolds to ignore.
    log : file object
        A file handle for the output log.
    """
    fseq_recs = []
    kept_genes = set()
    for rec in seq_recs:
        # Get the landmark sequence record.
        seqid = rec.id

        # Skip scaffolds that we want to ignore.            
        if seqid in ignore:
            log.write("Ignore models on landmark {}\n".format(seqid))
            continue
        
        print("Correcting genes models after filtering in '{}'...".format(seqid))
        seq_rec = copy.deepcopy(getSeqRecord(seqid))

        # Iterate through the features (i.e. genes).
        for feature in rec.features:
            if feature.type == 'gene':                
                gene = feature # type: Bio.SeqFeature.SeqFeature

                # First, fix the gene start/stop coordinates.
                # Iterate through the subfeatures of the gene.
                gstart = gene.location.end
                gend = gene.location.start            
                for subfeature in gene.sub_features:
                    if subfeature.type == 'mRNA':
                        mRNA = subfeature
                        if mRNA.location.start < gstart:
                            gstart = mRNA.location.start
                        if mRNA.location.end > gend:
                            gend = mRNA.location.end
                gene.location = FeatureLocation(start=gstart, end=gend, 
                                                strand=gene.strand, ref=gene.ref)

                # Second, fix the transcript IDs to have
                # successively increasing IDs.
                mRNA_index = 1
                for subfeature in gene.sub_features:
                    if subfeature.type == 'mRNA':
                        mRNA = subfeature
                        mRNA_id = mRNA.id
                        mRNA_id = re.sub(r"\d+$", str(mRNA_index), mRNA_id)
                        if not (mRNA_id == mRNA.id):                            
                            log.write("Renaming {} as {}\n".format(mRNA_id, mRNA.id))                        
                            mRNA.id = mRNA_id
                            mRNA.qualifiers['ID'] = mRNA_id                            
                        
                        # Now fix subfeature parents and IDs
                        for subsubfeature in mRNA.sub_features:
                            subsubfeature.qualifiers['Parent'] = ''
                            sub_id = subsubfeature.id
                            sub_id = re.sub(r"\.t\d+\.", ".t{}.".format(str(mRNA_index)), sub_id)
                            subsubfeature.id = sub_id
                            subsubfeature.qualifiers['ID'] = sub_id              

                        mRNA_index = mRNA_index + 1


            seq_rec.features.append(gene)     

        # Add this landmark sequence to the full list.
        fseq_recs.append(seq_rec)

    # Return our new list of landmark sequences with features.
    return fseq_recs     

def filterRepeatOverlaps(args, seq_recs, gene_locs, log):
    """
    Removes genes with too much overlap with repeats

    Parameters
    ----------
    args : args object
        Command-line arguments for this program.
    seq_recs : array
        An array of SeqRecords containing gene features.
    gene_locs : dict
        A dictionary of gene positions.
    log : file object
        A file handle for the output log.

    Returns
    -------
    fseq_recs : array
        An array of SeqRecords containing gene features.

    """
    fseq_recs = []
    kept_genes = set()
    for rec in seq_recs:

        # Get the landmark sequence record.
        seqid = rec.id
        print("Checking genes for overlap with repeats '{}'...".format(seqid))
        seq_rec = copy.deepcopy(getSeqRecord(seqid))

        # Iterate through the features (i.e. genes).
        for feature in rec.features:
            if feature.type == 'gene':
                gene = feature
                bad_mRNAs = []
                feature_index = 0

                # Iterate through the subfeatures of the gene.
                for subfeature in gene.sub_features:
                    if subfeature.type == 'mRNA':
                        mRNA = subfeature
                        num_cds = 0
                        total_overlap = 0
                        CDS = ""

                        # Iterate through the subfeatures of the mRNA
                        for subsubfeature in mRNA.sub_features:
                            if subsubfeature.type == 'CDS':
                                cds = subsubfeature
                                num_cds = num_cds + 1
                                cds_start = cds.location.start
                                cds_end = cds.location.end
                                CDS = CDS + str(seq_rec.seq[cds_start:cds_end])
                                numX = seq_rec.seq.count('a', cds_start, cds_end) + \
                                       seq_rec.seq.count('c', cds_start, cds_end) + \
                                       seq_rec.seq.count('t', cds_start, cds_end) + \
                                       seq_rec.seq.count('g', cds_start, cds_end)

                                # Calculate the amount of overlap
                                total_overlap = total_overlap + numX

                        if total_overlap/len(CDS) > 0.9:
                            bad_mRNAs.append(feature_index)
                            log.write("Removing mRNA {}:{}. Repeat overlap: {}. Limit: {}\n".format(seqid, mRNA.id, total_overlap, args.max_rbppe))
                            log.write(CDS + "\n")
                        else:
                            log.write("Keeping mRNA {}:{}. Repeat overlap: {}. Limit: {}\n".format(seqid, mRNA.id, total_overlap, args.max_rbppe))

                    feature_index = feature_index + 1

                # If we have any mRNA in this gene then add it to the
                # sequence record.
                if len(bad_mRNAs) > 0:
                    bad_mRNAs.sort(reverse=True)
                    for feature_index in bad_mRNAs:
                        del gene.sub_features[feature_index]
                if len(gene.sub_features) > 0:
                    gene_start = gene.location.start
                    gene_end = gene.location.end
                    gene_strand = gene.strand

                    # Update the gene location in case we've removed some mRNA.
                    for subfeature in gene.sub_features:
                        if subfeature.location.start < gene_start:
                            gene_start = subfeature.location.start
                        if subfeature.location.end > gene_end:
                            gene_end = subfeature.location.end
                    gene.location = FeatureLocation(gene_start, gene_end)
                    gene.strand = gene_strand
                    seq_rec.features.append(gene)
                    kept_genes.add(gene.id)

        # Add this landmark sequence to the full list.
        fseq_recs.append(seq_rec)

    # Return our new list of landmark sequences with features.
    return [fseq_recs, kept_genes]



def splitBadModels(args, seq_recs, log):
    """
    Removes genes that have transcripts that don't span most of the model.

    Parameters
    ----------
    args : args object
        Command-line arguments for this program.
    seq_recs : array
        An array of SeqRecords containing gene features.
    log : file object
        A file handle for the output log.

    Returns
    -------
    fseq_recs : array
        An array of SeqRecords containing gene features.

    """   

    fseq_recs = []
    kept_genes = set()
    for rec in seq_recs:
        seqid = rec.id
        print("Checking genes for odd transcripts '{}'...".format(seqid))
        seq_rec = SeqRecord(seq=Seq(""), id=rec.id)
        
        for feature in rec.features:
            if feature.type == 'gene':
                gene = feature
                mRNA_starts = {}
                mRNA_ends = {}
                num_mRNA = 0
                
                # Find the start and stop coordinates for each transcript.
                for subfeature in sorted(gene.sub_features, key=attrgetter('location.start')):
                    if subfeature.type == 'mRNA':
                        num_mRNA = num_mRNA + 1
                        mRNA = subfeature
                        
                        # Calculate where CDS start and end in this mRNA
                        mcds_start = int(mRNA.location.end)
                        mcds_end = int(mRNA.location.start)
                        for subsubfeature in mRNA.sub_features:
                            if subsubfeature.type == 'CDS':
                                cds = subfeature
                                cds_start = int(cds.location.start)
                                cds_end = int(cds.location.end)
                                if cds_start < mcds_start:
                                    mcds_start = cds_start
                                if cds_end > mcds_end:
                                    mcds_end = cds_end
                                    
                        mRNA_starts[mRNA.id] = mcds_start
                        mRNA_ends[mRNA.id] = mcds_end
                        
                # Look to see if there are any transcripts with no overlaps
                # with others. If so, the gene model is suspect. First generate
                # an empty associate array to keep track of which transcripts overlap.
                overlaps = {}
                for x in range(len(mRNA_starts)):
                    mRNA_id = list(mRNA_starts.keys())[x];
                    overlaps[mRNA_id] = set()
                
                # Pairwise compare each transcript to see if they overlap. If they
                # do then store them in the overlap array.  If a transcript
                # does not overlap with any other transcript then store it in the 
                # split set.
                split = set()
                for x in range(len(mRNA_starts)):
                    mRNA_id_x = list(mRNA_starts.keys())[x];                    
                    for y in range(x + 1, len(mRNA_starts)):
                        mRNA_id_y = list(mRNA_starts.keys())[y];
                        if (mRNA_ends[mRNA_id_x] > mRNA_starts[mRNA_id_y]):
                            overlaps[mRNA_id_x].add(mRNA_id_y)
                            overlaps[mRNA_id_y].add(mRNA_id_x)
                    if len(overlaps[mRNA_id_x]) == 0:
                        split.add(mRNA_id_x)

                #if gene.id == 'g1774':
                #    print(mRNA_starts)
                #    print(mRNA_ends)
                #     print(overlaps)
                #     print(split)
                #     print(num_mRNA)
                            
                # If the gene is not bad then keep it.
                if num_mRNA == 1 or len(split) == 0:
                    seq_rec.features.append(gene)  
                    kept_genes.add(gene.id)
                else:                                

                    i = 0
                    kept_transcripts = []
                    for subfeature in gene.sub_features:
                        if subfeature.type == 'mRNA':
                            mRNA = subfeature
                            if mRNA.id in split:
                                
                                gene_id = gene.id + ".split-" + str(i)
                                mRNA.id = gene_id + ".t1"
                                mRNA.qualifiers['ID'] = mRNA.id
                                mRNA.qualifiers['Name'] = mRNA.id
                                mRNA.qualifiers['Parent'] = ''

                                # Create a new gene for this transcript.
                                qualifiers = {'ID': gene_id, 'Name': gene_id}
                                new_gene = SeqFeature(location=mRNA.location, type=gene.type, 
                                                    strand=mRNA.strand, id=gene_id,
                                                    ref=gene.ref, qualifiers=qualifiers) 
                                new_gene.qualifiers['ID'] = gene_id
                                new_gene.qualifiers['Name'] = gene_id
                                new_gene.sub_features = []
                                new_gene.sub_features.append(mRNA);
                                log.write("Splitting gene {}. Adding new gene for transcript {} -> {}\n".format(gene.id, mRNA_id, mRNA.id))
                                seq_rec.features.append(new_gene)  
                                kept_genes.add(new_gene.id)
                                i = i + 1
                            else:
                                kept_transcripts.append(mRNA)
                    
                    # Keep the original gene (minus split transcripts) if we need to.
                    if len(kept_transcripts) > 0:
                        new_gene = SeqFeature(location=gene.location, type=gene.type, 
                                              strand=gene.strand, id=gene.id,
                                              ref=gene.ref, qualifiers={}) 
                        new_gene.qualifiers['ID'] = gene.id
                        new_gene.qualifiers['Name'] = gene.id
                        new_gene.sub_features = []
                        for mRNA in kept_transcripts:
                            new_gene.sub_features.append(mRNA);
                        seq_rec.features.append(new_gene)  
                        kept_genes.add(new_gene.id)

        # Add this landmark sequence to the full list.
        fseq_recs.append(seq_rec)
    
    return [fseq_recs, kept_genes]
                        
                            
                            
    
def filterGeneOverlaps(args, seq_recs, gene_locs, kept_genes, log):
    """
    Filters genes that overlap with other genes.

    Parameters
    ----------
    args : args object
        Command-line arguments for this program.
    seq_recs : list
        A list of SeqRecords containing gene features.
    gene_locs : dict
        A dictionary of gene positions.
    kept_genes : set
        The set of gene IDs that were kept from the repeat overlap filtering.
    log : file object
        A file handle for the output log.

    Returns
    -------
    fseq_recs : array
        An array of SeqRecords containing gene features.

    """
       
    fseq_recs = []
    keep = set()
    skip = set()
    for rec in seq_recs:

        # Get the landmark sequence record.
        seqid = rec.id
        print("Checking genes for overlap with other genes in '{}'...".format(seqid))
        seq_rec = SeqRecord(seq=Seq(""), id=rec.id)

        # Iterate through the features (i.e. genes).
        for feature in rec.features:

            if feature.type == 'gene':
                gene = feature 
                start = gene_locs[gene.id][1]
                end = gene_locs[gene.id][2]
                
                # If we've already seen this gene because it overlaps with
                # another one and it was decided that we should keep it, then
                # add it to the SeqRecord feature list and move to the next .                
                if gene.id in keep:
                    log.write("Saving {} at {}:{}-{}. It was found to the be the best in a previous overlap comparision.\n".format(gene.id, seqid, start, end))
                    seq_rec.features.append(gene)
                    continue
                
                # If we've already seen this gene because it overlaps with
                # another one and has been rejected in favor of the other one
                # then skip it.
                if gene.id in skip:
                    log.write("Skipping {} at {}:{}-{}. It was rejected in a previous comparison.\n".format(gene.id, seqid, start, end))
                    continue;

                # Get the gene locations on this landmark.
                seqglocs = []
                if seqid in gene_locs.keys():
                    seqglocs = gene_locs[seqid]
                
                # Determine which genes overlap.
                gene_overlaps = []
                for tstart in seqglocs.keys():
                    for tend in seqglocs[tstart].keys():
                        
                        # Don't check the same gene with itself.
                        if gene.id in seqglocs[tstart][tend]:
                            continue
                                                
                        is_overlap = False
                        
                        # Different types of overlaps
                        # Case:  1    2    3   4     5     6
                        # G:   ---- ---- ---- ----   ---- ----
                        # T:   ---- ---   ---  --  ----     ----
                        
                        # Matches cass: 1, 2, 4, 6
                        if (start <= tstart)  and (end > tstart):
                            is_overlap = True
                        # Matches cases: 1, 3, 4, 5
                        if (end >= tend)  and (start < tend):
                            is_overlap = True                 
                            
                        # Get the IDs of genes that overlap.
                        if is_overlap:
                            for tgid in seqglocs[tstart][tend]:
                                
                                # Only continue with genes that are present 
                                # after previous filtering.
                                if tgid in kept_genes:
                                    log.write("Gene {} overlaps with {}\n".format(gene.id, tgid))                                    
                                    
                                    # If a gene has already been skipped from a 
                                    # previous comparision then skip it here too.
                                    if tgid in skip:
                                        log.write("Gene {} overlaps with {} but ignoring it because it was rejected in a previous comparison.\n".format(gene.id, tgid))
                                    else:
                                        gene_overlaps.append(tgid)
                                
                # If there are overlapping genes then decided which to keep.
                if (len(gene_overlaps) > 0):
                    log.write("Overlap between {} and {}.\n".format(gene.id, gene_overlaps))
                    # Add this gene to the list so we can loop on it.
                    gene_overlaps.append(gene.id)
                    
                    # Find gene with the highest RNA-seq alignment evidence.
                    best_model = None
                    highest_evidence = 0
                    selected = []                    
                    for gid in gene_overlaps:
                        num_reads = gene_locs[gid][3]
                        if num_reads == highest_evidence:
                            selected.append(gid)
                        if num_reads > highest_evidence:
                            best_model = gid
                            highest_evidence = num_reads
                            selected = []
                            selected.append(gid)
                            
                    # If one gene has the highest RNA-seq evidnece
                    if len(selected) == 1:                        
                        # If this gene is the one selected then add it to 
                        # SeqRecord features list.
                        if best_model == gene.id:
                            seq_rec.features.append(gene)
                            log.write("  Saving {} at {}:{}-{}. It has the highest RNA-seq alignments: {}.\n".format(gene.id, seqid, start, end, highest_evidence))
                        # Its not the current gene so keep it for later when
                        # we loop to it.
                        else:
                            log.write("  Keeping {} for later. It has the highest RNA-seq alignments: {}.\n".format(best_model, highest_evidence))
                            keep.add(best_model)
                         
                        # For those not selected mark them for skipping later.    
                        for gid in gene_overlaps:
                            if gid != best_model:
                                skip.add(gid)
                            
                    # If we have more than one gene with the same level
                    # of RNA-seq evidence then select the one with the longest
                    # CDS 
                    else:
                        log.write("  These genes have the same level of RNA-seq evidence: {}.\n".format(selected))
                        cds_selected = []
                        best_model = None
                        bestcds = []
                        highest_cdslen = 0;
                        for gid in selected: 
                            cds_len = gene_locs[gid][4]
                            if cds_len == highest_cdslen:
                                cds_selected.append(gid)
                            if cds_len > highest_cdslen:
                                best_model = gid
                                highest_cdslen = cds_len
                                cds_selected = []
                                cds_selected.append(gid)
                        
                        # Add the gene with the longest CDS. If there are 
                        # more than one than just select the first.
                        if len(cds_selected) > 1:                            
                            best_model = cds_selected[0]
                            log.write("  Multiple genes have the same size CDSs. Selecting this one: {}.\n".format(best_model))
                        if best_model == gene.id:
                            log.write("  Saving {} at {}:{}-{}. It was selected for CDS length: {}.\n".format(gene.id, seqid, start, end, highest_cdslen))
                            seq_rec.features.append(gene)
                        else:
                            log.write("  Keeping {} for later. It was selected for CDS length: {}.\n".format(best_model, highest_cdslen))
                            keep.add(best_model)
                           
                        # For those not selected mark them for skipping later.    
                        for gid in gene_overlaps:
                            if gid != best_model:
                                skip.add(gid)
                            
                            
                
                # Else we have no overlaps, add this genne to the SeqRecord
                # features list.
                else:
                    seq_rec.features.append(gene)
                    
        # Add this landmark sequence to the full list.
        fseq_recs.append(seq_rec)
        
    return fseq_recs
                
          
    

def main():
    """
    The main function
    """
    # Specify the arguments that are allowed by this script
    parser = argparse.ArgumentParser()
    parser.add_argument('--genome', dest='genome', action='store', required=True,
                        help='The path to the softmasked whole file FASTA file.')
    parser.add_argument('--bamfile', dest='bamfile', action='store', required=True,
                        help='The path to the RNAseq alignments in BAM format.')
    parser.add_argument('--genes', dest='genes', action='store', required=True,
                        help='The path to the GFF file with the gene annotations.')
    parser.add_argument('--max_rbppe', dest='max_rbppe', action='store', type=int, default=300,
                        help='The maximum number of repeat bases. This is meant to ignore small simple repeats or LTRs.')
    parser.add_argument("--ignore", dest='ignore', action='store', required=False,
                        help='The path to a file containing landmark sequences to ignore. Any gene models on these scaffolds will not be kept.')

    # Parse the arguments provided by the user.
    args = parser.parse_args()

    # same basename as a prefix for all of the output files.
    basename = os.path.basename(args.genes)
    basename = os.path.splitext(basename)[0]

    # Open the input files
    genes_in = open(args.genes, "r")
    genome_in = open(args.genome, "r")

    # Open the output files
    genes_out_gff = open(basename + '.filtered.gff3', "w")
    log = open(basename + '.filtered.log', "w")

    # Open the ignore file
    ignore = {}
    if (args.ignore):
      ignore = pd.read_csv(args.ignore, header=None)[0].values

    # Parse the Genome sequence file
    print("Reading Genome File...")
    genome = SeqIO.parse(genome_in, "fasta")
    parseGenome(genome)

    # Parse the GFF file. To make rerunning of this script faster
    # we'll pickle the data and reload next time.
    seq_recs_pfh = basename + ".seqs.p" 
    seq_recs = []
    if not exists(seq_recs_pfh):
        print("Parsing GFF File...")    
        seq_recs = parseGFF(genes_in)
        pickle.dump(seq_recs, open(seq_recs_pfh, "wb"))
    else:
        print("Found seq reqs file {}. Delete this file to rebuild it.".format(seq_recs_pfh))
        seq_recs = pickle.load(open(seq_recs_pfh, "rb"))

    # Get information about the genes. To make rerunning of this script faster
    # we'll pickle the data and reload next time.
    bam_in = pysam.AlignmentFile(args.bamfile, "rb")
    gene_locs_pfh = basename + ".locs.p" 
    gene_locs = {}
    if not exists(gene_locs_pfh):
        print("Generating gene location and evidence data...")    
        gene_locs = getGeneLocs(seq_recs, bam_in)    
        pickle.dump(gene_locs, open(gene_locs_pfh, "wb"))
    else:
        print("Found gene loc file {}. Delete this file to rebuild it.".format(gene_locs_pfh))
        gene_locs = pickle.load(open(gene_locs_pfh, "rb"))

        

    # Filters genes
    seq_recs2, kept_genes1 = filterRepeatOverlaps(args, seq_recs, gene_locs, log)    
    seq_recs3, kept_genes2 = splitBadModels(args, seq_recs2, log)
    #seq_recs4 = filterGeneOverlaps(args, seq_recs3, gene_locs, kept_genes2, log)
    #seq_recs5 = filterDuplicateTranscripts(args, seq_recs4, log)
    seq_recs6 = fixGenes(seq_recs3, ignore, log)
    
    # Write out the final gene list.
    print("Writing GFF file...");
    GFF.write(seq_recs6, genes_out_gff)

    # Close the file handles
    genes_out_gff.close()
    genes_in.close()
    log.close()


if __name__ == "__main__":
    main()

