## Filtering

Link to the genome sequence file
```bash
ln -s ../../../04-nuclear_assembly/11-filtering/Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa
ln -s ../../../04-nuclear_assembly/11-filtering/Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa
```

Link to the softmasked assembly files. These will be used by the filtering script
To know where repeats occur.

```bash
ln -s ../../01-annotate_repeats/02-RepeatMasker/01-hapA/Malus-domestica-WA_38_hapA-genome-v1.0.a1.softmasked.fa
ln -s ../../01-annotate_repeats/02-RepeatMasker/02-hapB/Malus-domestica-WA_38_hapB-genome-v1.0.a1.softmasked.fa
```

Linked to the RNA-seq aligned reads
```bash
ln -s ../05-Trinity/01-hapA/RNAseq_all-vs-WA_38-hapA.5perc.sorted.bam
ln -s ../05-Trinity/01-hapA/RNAseq_all-vs-WA_38-hapA.5perc.sorted.bam.bai 
ln -s ../05-Trinity/02-hapB/RNAseq_all-vs-WA_38-hapB.5perc.sorted.bam
ln -s ../05-Trinity/02-hapB/RNAseq_all-vs-WA_38-hapB.5perc.sorted.bam.bai
```

Link to the most recent PASA updated GFF3 file
```bash
ln -s `ls -t ../06-PASA/01-hapA/*.gene_structures_post_PASA_updates*.gff3 | head -n 1` WA_38.gene_structures_post_PASA_updates.hapA.gff3
ln -s `ls -t ../06-PASA/02-hapB/*.gene_structures_post_PASA_updates*.gff3 | head -n 1` WA_38.gene_structures_post_PASA_updates.hapB.gff3

```

Now run the filtering scripts:

```bash
01-filter-hapA.srun
02-filter-hapB.srun
```
