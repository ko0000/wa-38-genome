# BUSCO version is: 5.4.3 
# The lineage dataset is: eudicots_odb10 (Creation date: 2020-09-10, number of genomes: 31, number of BUSCOs: 2326)
# Summarized benchmarking in BUSCO notation for file /scidas/instruction/HORT503-Fall2022/wa-38-wgaa-official_files/05-structural_annotation/02-annotate-genes/08-Files/Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.faa
# BUSCO was run in mode: proteins

	***** Results: *****

	C:98.4%[S:46.0%,D:52.4%],F:0.5%,M:1.1%,n:2326	   
	2288	Complete BUSCOs (C)			   
	1070	Complete and single-copy BUSCOs (S)	   
	1218	Complete and duplicated BUSCOs (D)	   
	11	Fragmented BUSCOs (F)			   
	27	Missing BUSCOs (M)			   
	2326	Total BUSCO groups searched		   

Dependencies and versions:
	hmmsearch: 3.1
	busco: 5.4.3
