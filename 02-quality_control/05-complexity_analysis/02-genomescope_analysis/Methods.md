Upload the WA38.counts.histo file to http://qb.cshl.edu/genomescope/

The following form settings were used:
- Description: WA38
- kmer-length: 21
- Read length: 151
- Max kmer coverage: 1000


The report is accessible here:

http://genomescope.org/analysis.php?code=dfhaYKCO66rBVzCT4hwE

Download images for preservation (Right click on images to get URL):

```bash
wget http://qb.cshl.edu/genomescope/user_data/dfhaYKCO66rBVzCT4hwE/plot.png
wget http://qb.cshl.edu/genomescope/user_data/dfhaYKCO66rBVzCT4hwE/plot.log.png
```