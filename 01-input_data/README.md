Directory Overview
==================
This directory includes input data and some siftware used for the project.  
Authors: Huiting Zhang  
Last updated: Jan 3rd 2024  

Directory Structure
===================
The following describes the purpose for each directory:
- bioawk

This tool is used in sequence format manipulation and sequence characteristics analysis.  
Below is the code used to install bioawk

- functional_data  
Databases downloaded with EnTAPnf for functioanl annotation.  

- GeneMark  
GeneMark was downloaded to run BRAKER in the structural annotation step.  

- Illumina  
Illumina short read sequences.  
    - Hi-C  
    - RNAseq  
    - Shotgun-DNA  

- OrthoDB  
Protein sequences from virdiplantea orthoDBv11 was downloaded to use as protein evidence for BRAKER annotation.  

- orthogroup_data  
PlantTribes2 orthogroup database 26Gv2.0 was downloaded to serve as the pre-computed orthogroups for classifying genomes of interest in the comparative genomics step.  

- PacBio  
PacBio HiFi data

- Rosaceae_proteinDB  
Protein sequences from selected Rosaceae genomes downloaded to use as protein evidence for BRAKER annotation.  

- TRINITY_assembly  
de novo transcriptome assemblies from the Honeycrisp genome paper. The transcripts are used to facilitate PASA annotation. The proteins are used as protein evidence for BRAKER annotation.  
    - proteins  
    - transcripts  