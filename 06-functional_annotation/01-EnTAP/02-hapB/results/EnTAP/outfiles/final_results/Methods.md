Since the InterProScan results are incorrectly incorporated into the final result table, let's remove those columns and make a new table.

```bash
cut -f1-37 final_annotations_no_contam_lvl0.tsv > Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.EnTAP.annotation.tsv
```