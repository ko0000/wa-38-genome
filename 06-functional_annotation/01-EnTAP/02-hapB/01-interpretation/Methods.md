The goal of this analysis is to calculate how many proteins were assigned with functional terms from each program (and all the programs) used by EnTAP.

First, count the number of contaminants
```bash
grep -c ">" ../results/EnTAP/outfiles/final_results/final_annotations_contam.faa 
#0 This is no contamination detected by EnTAP
```
Next, count the number of annotated and unannotated proteins
```bash
grep -c ">" ../results/EnTAP/outfiles/final_results/final_annotated.faa 
#62595
grep -c ">" ../results/EnTAP/outfiles/final_results/final_unannotated.faa      
#7809
```

The percentage of genes with annotation (from one or more tools) is: 62595/(62595+7809)=88.91%  

Finally, count how many genes were annotated by each tool  

Create a symlink of the EnTAP result file
```bash
ln -s ../results/EnTAP/outfiles/final_results/Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.EnTAP.annotation.tsv
```

Now seperate results from each tool
```bash
cut -f3 Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.EnTAP.annotation.tsv | sed '/^\s*$/d' | wc -l
#   58455. 58454 proteins are annotated by BLAST (the count of 58455 included header line)
cut -f19 Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.EnTAP.annotation.tsv | sed '/^\s*$/d' | wc -l
#    7640. 7639 proteins are annotated by UniProt search
cut -f25 Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.EnTAP.annotation.tsv | sed '/^\s*$/d' | wc -l
#   60700. 60699 proteins are annotated by EggNOG seed ortholog search
cut -f33 Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.EnTAP.annotation.tsv | sed '/^\s*$/d' | wc -l
#   14300. 14299 proteins are annotated by EggNOG KEGG search
cut -f34 Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.EnTAP.annotation.tsv | sed '/^\s*$/d' | wc -l
#   43081. 43080 proteins are annotated by EggNOG GO biological process search
cut -f35 Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.EnTAP.annotation.tsv | sed '/^\s*$/d' | wc -l
#   36925. 36924 proteins are annotated by EggNOG GO cellular component search
cut -f36 Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.EnTAP.annotation.tsv | sed '/^\s*$/d' | wc -l
#   41440. 41439 proteins are annotated by EggNOG GO molecular function search
cut -f37 Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.EnTAP.annotation.tsv | sed '/^\s*$/d' | wc -l
#   57270. 57269 proteins are annotated by EggNOG Protein Domain search
```

The output from InterProScan was integrated incorrectly in the final file, therefore, we need to analyze that using the files from the InterProScan output directory
```bash
ln -s ../results/InterProScan/Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.cleaned.tsv 
ln -s ../results/InterProScan/Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.cleaned.GO_mappings.txt 
ln -s ../results/InterProScan/Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.cleaned.IPR_mappings.txt 
```

Count how many genes have GO term annotation
```bash
cut -f1 Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.cleaned.GO_mappings.txt| grep Maldo | sort | uniq | wc -l
#   35886
```

Count how many genes have IPR terms
```bash
cut -f1 Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.cleaned.IPR_mappings.txt | grep Maldo | sort | uniq | wc -l 
#   50975
```

Count how many genes have hits in all the databases searched by InterProScan
```bash
cut -f1 Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.cleaned.tsv | grep Maldo | sort | uniq | wc -l 
#   59577
```